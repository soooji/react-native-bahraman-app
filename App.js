import React from 'react';
import { Text, View, Platform, StyleSheet, StatusBar, Button, TouchableOpacity } from 'react-native';
import { createBottomTabNavigator, createAppContainer,createStackNavigator ,createSwitchNavigator} from 'react-navigation';
import TabBarComponent from './components/Main/TabBarComponent'
import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'
//pages
import BahramanScreen from './pages/Bahraman/index'
import CookingScreen from './pages/CookingScreen'
import Govahiname from './pages/Bahraman/Govahiname'
import AboutBahraman from './pages/Bahraman/AboutBahraman'
import Products from './pages/Bahraman/Products'
import Call from './pages/Bahraman/Call'
import News from './pages/Bahraman/News'
import Quality from './pages/Bahraman/Quality'
import NavigationService from './navigation-service';
const BahramanNavigator = createStackNavigator(
  {
    BahramanPage: {screen: BahramanScreen,params: { title: 'بهرامن' }},
  },
  {
    initialRouteName: 'BahramanPage',
    defaultNavigationOptions : {
      header : props => <View style={[{position:'relative',backgroundColor:'white'},styles.topBar]}><Text style={[{color:'#969696'},styles.topBarText]}>
      {props.scene.route.params.title}
      </Text>
      <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
      onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
        <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      </View>
    }
  }
);
 const CookingNavigator = createStackNavigator(
  {
    CookingPage: {screen: CookingScreen,params: { title: 'اشپزی' }},
  },
  {
    initialRouteName: 'CookingPage',
    defaultNavigationOptions : {
      header : props => <View style={[{position:'relative',backgroundColor:'red'},styles.topBar]}><Text style={[{color:'#969696'},styles.topBarText]}>
      {props.scene.route.params.title}
      </Text>
      <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
      onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
        <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      </View>
    }
  }
);


const aboutNavigator = createStackNavigator(
  {
    aboutBahraman: {screen: AboutBahraman,params: { title: 'درباره ی بهرامن' }},
  },
  {
    initialRouteName: 'aboutBahraman',
    defaultNavigationOptions : {
      header : props => <View style={[{position:'relative',backgroundColor:'white'},styles.topBar]}><Text style={[{color:'#969696'},styles.topBarText]}>
      {props.scene.route.params.title}
      </Text>
      <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
      onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
        <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      </View>
    }
  }
);

const TabNavigator = createBottomTabNavigator({
  Bahraman: BahramanNavigator,
  Settings: CookingNavigator,
  Appnav: Appnavigator
},{
  tabBarComponent: props =>
  <TabBarComponent
    {...props}
  />
},{navigationOptions: () => ({header: null})}
);

const appnavigator = createStackNavigator({
  BahramanScreen:{screen:TabNavigator},
  Govahiname : {screen : Govahiname},
  aboutBahraman :{screen:aboutNavigator },
  products :{screen:Products,},
  call:{screen :Call},
  news :{screen :News},
  quality :{screen:Quality}
  
},
// {
//   // TabBarComponent: TabNavigator
// },
  {navigationOptions: () => ({header: null}),},
    {initialRouteName: 'Govahiname'}
)
// const FullStack = createSwitchNavigator(
//   {
//   x:TabNavigator,
//   y:appnavigator
//   }
// )
const AppContainer = createAppContainer(appnavigator);
class App extends React.Component {
  render() {
    return (
      <AppContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}



styles = StyleSheet.create({
  topBarText: {
    // fontFamily:'Mj_Flow',
    fontSize:RF(2.4),
    textAlign:'center'
  },
  topBar: {
    width:'100%',
    paddingHorizontal:10,
    paddingTop:Platform.OS == "ios" ? 38 : StatusBar.currentHeight,
    paddingBottom:10
  }
})
export default App;
// export default AppContainer;
