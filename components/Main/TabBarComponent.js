import React, { Component } from 'react'
import {StyleSheet, Text, View,TouchableOpacity,Dimensions} from 'react-native'
const WIN = Dimensions.get('window')
import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'

export default class TabBarComponent extends Component {
    
  render() {
    return (
      <View style={[styles.tabBar,styles.FLEX_RR_N_SB_C_S]}>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("Bahraman")} activeOpacity={.8} style={styles.tabBarItem}>
            <Icon name="star" color={this.props.navigation.state.index == 0 ? "#f2bcd7" :"#959595"} style={styles.tabBarIcon}/>
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("Settings")} activeOpacity={.8} style={styles.tabBarItem}>
            <Icon name="star" color={this.props.navigation.state.index == 1 ? "#f2bcd7" :"#959595"} style={styles.tabBarIcon}/>
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <TouchableOpacity activeOpacity={.8} style={styles.tabBarItem}>
            <Icon name="star" color={this.props.navigation.state.index == 2 ? "#f2bcd7" :"#959595"} style={styles.tabBarIcon}/>
        </TouchableOpacity>
        <View style={styles.spacer}></View>
        <TouchableOpacity activeOpacity={.8} style={styles.tabBarItem}>
            <Icon name="star" color={this.props.navigation.state.index == 3 ? "#f2bcd7" :"#959595"} style={styles.tabBarIcon}/>
        </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
    spacer: {
        height:'50%',
        backgroundColor:'#eeeeee',
        width:2,
        borderRadius:10
    },
    tabBarIcon: {
        fontSize: RF(3.5),
        textAlign:'center'
    },
    tabBarItem: {
        paddingVertical:20,
        width: (WIN.width-20)/4
    },
    tabBar: {
        backgroundColor:'rgb(255, 255, 255)',
        width:'100%',
        shadowRadius:20,
        shadowOpacity:.4,
        shadowColor:'#b7b7b7',
        shadowOffset:{width:0,height:-14},
        paddingBottom:10
    },
    FLEX_RR_N_SB_C_S :{
        display: 'flex',
        flexDirection: 'row-reverse',
        flexWrap: 'nowrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'stretch',
    }
})