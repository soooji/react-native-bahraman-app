import React, { Component } from 'react'
import { Text, View , Image, Dimensions, StyleSheet, ScrollView,TouchableOpacity} from 'react-native'
const WIN = Dimensions.get('window')

import AutoHeightImage from 'react-native-auto-height-image';

const ZaferanName = require("./../../static/images/drawer/zaferan-name.png");
const AboutCreator = require("./../../static/images/drawer/about-creator.png");
const About = require("./../../static/images/drawer/about.png");
const Documents = require("./../../static/images/drawer/documents.png");
const Gallery = require("./../../static/images/drawer/gallery.png");
const Kitchen = require("./../../static/images/drawer/kitchen.png");
const Knowledge = require("./../../static/images/drawer/knowledge.png");

export default class componentName extends Component {
  render() {
    return (
        <ScrollView style={styles.container} bounces={false} stickyHeaderIndices={[0]}>
            {/* size of drawer equals to : WIN.width/100*80 */}
            <View style={styles.ZaferanNameBg}>
                <AutoHeightImage width={(WIN.width/100)*60} style={styles.ZaferanNameLogo} source={ZaferanName}/>
            </View>
            
            <View style={[styles.Flex_R_N_SE_B_S,styles.MenuRow]}>
                <TouchableOpacity><AutoHeightImage width={(WIN.width/100*70)/2} source={Knowledge}/></TouchableOpacity>
                <TouchableOpacity><AutoHeightImage width={(WIN.width/100*70)/2} source={Documents}/></TouchableOpacity>
            </View>
            <View style={[styles.Flex_R_N_SE_B_S,styles.MenuRow]}>
                <TouchableOpacity><AutoHeightImage width={(WIN.width/100*73)} source={Kitchen}/></TouchableOpacity>
            </View>
            <View style={[styles.Flex_R_N_SE_B_S,styles.MenuRow]}>
                <TouchableOpacity><AutoHeightImage width={(WIN.width/100*70)/2} source={About}/></TouchableOpacity>
                <TouchableOpacity><AutoHeightImage width={(WIN.width/100*70)/2} source={Gallery}/></TouchableOpacity>
            </View>
            <View style={[styles.Flex_R_N_SE_B_S,styles.MenuRow,{paddingBottom:10}]}>
            <TouchableOpacity><AutoHeightImage width={(WIN.width/100*73)} source={AboutCreator}/></TouchableOpacity>
            </View>
        </ScrollView>
    )
  }
}
const styles = StyleSheet.create({
    container :{
        width:'100%',height:'100%',backgroundColor:'#5f0055'
    },
    MenuRow :{
        paddingHorizontal:(WIN.width/100*3)/2,paddingTop:10   
    },
    ZaferanNameBg: {
        width:'100%',paddingBottom:30,paddingTop:30,backgroundColor:'rgba(95,0,85,.8)'
    },
    ZaferanNameLogo: {
        marginLeft:'auto',marginRight:'auto'
    },
    Flex_R_N_SE_B_S: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        justifyContent: 'space-evenly',
        alignItems: 'baseline',
        alignContent: 'stretch'
    }
})